//Clovis Boucher 2133295
package studio.dreamys;

public class Cone implements Shape3d{

    private double radius;
    private Double height;

    public Cone(double radius, double height) {
        if(radius<=0||height<=0){
            throw new IllegalArgumentException("value cannot be negative");
        }
        this.radius = radius;
        this.height = height;
    }
    @Override
    public double getVolume() {
        return Math.PI*Math.pow(radius,2)*(height/3);
    }

    @Override
    public double getSurfaceArea() {
        return Math.PI*radius*(radius+Math.sqrt(Math.pow(height,2)+Math.pow(radius,2)));
    }


    
}
