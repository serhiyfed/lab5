package studio.dreamys;

/**
 * A cylinder shape
 * @author Serhiy Fedurtsya
 */
public class Cylinder implements Shape3d {
    private double radius;
    private double height;

    public Cylinder(double radius, double height) {
        if(radius <= 0 || height <= 0){
            throw new IllegalArgumentException("Radius and height must be positive");
        }

        this.radius = radius;
        this.height = height;
    }

    /**
     * @return Volume as a double
     */
    @Override
    public double getVolume() {
        return Math.PI * Math.pow(radius, 2) * height;
    }

    /**
     * @return Surface Area as a double
     */
    @Override
    public double getSurfaceArea() {
        return 2 * Math.PI * radius * (radius + height);
    }
}