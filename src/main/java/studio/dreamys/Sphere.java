package studio.dreamys;
import java.math.*;
/**
 * A sphere shape
 * @author Shuya Liu
 * */
public class Sphere implements Shape3d {
    private double radius;

    public Sphere(double radius){
        if(radius <= 0){
            throw new IllegalArgumentException("Radius must be over 0");
        }
        this.radius = radius;
    }

    @Override
    public double getVolume(){
        return Math.PI * Math.pow(radius,3);
    }

    @Override
    public double getSurfaceArea(){
        return 4 * Math.PI * Math.pow(radius, 2);
    }
}
