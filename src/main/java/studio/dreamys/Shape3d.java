package studio.dreamys;

public interface Shape3d {
    double getVolume();
    double getSurfaceArea();
}

