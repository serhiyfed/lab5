//Clovis Boucher 2133295
package studio.dreamys;

import static org.junit.Assert.*;

import org.junit.Test;

public class CylinderTest 
{
    @Test
    public void testConstructer(){
        try{
            new Cylinder(-3,0);
            fail();
        }catch(Exception e){

            e.printStackTrace();
        }
    }

    @Test
    public void testGetVolume(){
        Cylinder cylinder = new Cylinder(3,6);
        assertEquals(169.646003, cylinder.getVolume(),.000001);
    }
    
    @Test
    public void testGetSurfaceArea(){
        Cylinder cylinder = new Cylinder(3,6);
        assertEquals(169.646003, cylinder.getSurfaceArea(),.000001);
    }

}
