package studio.dreamys;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

/**
 * @author Serhiy
 */
public class SphereTest {
    @Test
    public void testConstructor() {
        try {
            new Sphere(0);
            fail();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testVolume() {
        Sphere sphere = new Sphere(3);
        assertEquals(27 * Math.PI, sphere.getVolume(), 0.001);
    }

    @Test
    public void testSurfaceArea() { 
        Sphere sphere = new Sphere(3);
        assertEquals(36 * Math.PI, sphere.getSurfaceArea(), 0.001);
    }
}
