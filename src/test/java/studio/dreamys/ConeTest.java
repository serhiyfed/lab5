package studio.dreamys;
import static org.junit.Assert.*;
import org.junit.Test;

public class ConeTest
{
    /**
     *@author Shuya
     */
    @Test
    public void constructorTest(){
        try {
            new Cone(-3.0, 4.0);
            fail();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Test
        public void getVolumeTest(){
            double number = 0.00000001;
            Cone myCone = new Cone(3.0, 4.0);
            assertEquals(37.6991118431, myCone.getVolume(), number);
        }

    @Test
    public void getSurfaceAreaTest()
    {   
        double number = 0.00000001;
        Cone myCone = new Cone(2.0, 3.0);
        assertEquals(35.2207174126, myCone.getSurfaceArea(), number);
    }
}